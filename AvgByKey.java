package com.greta.util;

import org.apache.beam.sdk.transforms.DoFn;
import org.apache.beam.sdk.values.KV;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Arrays;

/**
 * Created by bergqvist on 2017-10-11.
 */
public class AvgByKey extends DoFn<KV<String, Iterable<JsonDataStructure>>, Iterable<JsonDataStructure>> {
    private static final Logger LOG = LoggerFactory.getLogger(SumByKey.class);

    @ProcessElement
    public void processElement(ProcessContext c) {
        Iterable<JsonDataStructure> values = c.element().getValue();
        // iterate all the jsonobj, average
        JsonDataStructure first = values.iterator().next();

        if (!first.rollupSpec.isEmpty() && !first.rollupSpec.equals("null")) {
            String rollup = first.rollupSpec.replace("1h", "1d");
            rollup = rollup.replace("1m", "1h");
            JsonDataStructure res = new JsonDataStructure(0f, first.openTSDBKey, first.time, first.tags, rollup);

            Integer count = 0;
            for (JsonDataStructure obj : values) {
                res.value = res.value + obj.value;
                count = count + 1;
            }

            res.value = res.value / count;
            LOG.info("avgbykey, " + first.openTSDBKey + ", rollup from " + first.rollupSpec + " -> " + rollup);
            c.output(Arrays.asList(res));
        }
    }
}

