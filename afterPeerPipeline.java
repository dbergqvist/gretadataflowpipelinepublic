package com.greta;


import com.google.api.services.bigquery.model.TableFieldSchema;
import com.google.api.services.bigquery.model.TableRow;
import com.google.api.services.bigquery.model.TableSchema;
import com.greta.peer.ConcurrentDoFn;
import com.greta.util.*;
import org.apache.beam.sdk.Pipeline;
import org.apache.beam.sdk.io.gcp.bigquery.BigQueryIO;
import org.apache.beam.sdk.io.gcp.pubsub.PubsubIO;
import org.apache.beam.sdk.transforms.GroupByKey;
import org.apache.beam.sdk.transforms.PTransform;
import org.apache.beam.sdk.transforms.ParDo;
import org.apache.beam.sdk.transforms.windowing.SlidingWindows;
import org.apache.beam.sdk.transforms.windowing.Window;
import org.apache.beam.sdk.values.PCollection;
import org.joda.time.Duration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class PeerPipeline {
    private static final Logger LOG = LoggerFactory.getLogger(PeerPipeline.class);
    private static final String NAME = "peer";
    private static final int WINDOW_SIZE = 1;

    private static final String BIGQUERYTABLE = NAME + "Day";
    private static final String TABLESPEC = Start.PROJECTID + ":" + Start.DATASET + "." + BIGQUERYTABLE;
    private static final String PUBSUBNAME = "Read_" + NAME;
    private static final String TOPIC = "projects/" + Start.PROJECTID + "/topics/" + NAME;
    private static final String TOPICPUBSUB = ProjectTopicName.of(PROJECT_ID, topicId);

    static class CalculateConcurrent extends PTransform<PCollection<TableRow>, PCollection<Iterable<JsonDataStructure>>> {
        private final String rollup;

        CalculateConcurrent(String rollup) {
            this.rollup = rollup;
        }

        @Override
        public PCollection<Iterable<JsonDataStructure>> expand(PCollection<TableRow> infos) {
            PCollection<TableRow> res = infos.apply(rollup + "CalculateConcurrentSlidingWindows",
                    Window.<TableRow>into(SlidingWindows.of(
                            Duration.standardMinutes(4))));
            return res.apply(rollup + "_byAcc_concurrent", ParDo.of(new MapByPeer()))
                    .apply(rollup + "_groupbykey_concurrent", GroupByKey.<String, HashMap<String, String>>create())
                    .apply(rollup + "_pardo_concurrent", ParDo.of(new ConcurrentDoFn(rollup)));

        }
    }

    private static TableSchema getSchema() {
        List<TableFieldSchema> fields = new ArrayList<>();
        fields.add(new TableFieldSchema().setName("avgRtt").setType("INTEGER"));
        fields.add(new TableFieldSchema().setName("avgBandwidthP2P").setType("INTEGER"));
        fields.add(new TableFieldSchema().setName("avgBandwidthServer").setType("INTEGER"));
        fields.add(new TableFieldSchema().setName("avgConnectionTime").setType("INTEGER"));
        fields.add(new TableFieldSchema().setName("openConnections").setType("INTEGER"));
        fields.add(new TableFieldSchema().setName("failedConnections").setType("INTEGER"));
        fields.add(new TableFieldSchema().setName("timeouts").setType("INTEGER"));
        fields.add(new TableFieldSchema().setName("trackCallTime").setType("INTEGER"));
        fields.add(new TableFieldSchema().setName("isp").setType("STRING"));
        fields.add(new TableFieldSchema().setName("lat").setType("INTEGER"));
        fields.add(new TableFieldSchema().setName("lng").setType("INTEGER"));
        fields.add(new TableFieldSchema().setName("accessToken").setType("STRING"));
        fields.add(new TableFieldSchema().setName("time").setType("TIMESTAMP"));
        fields.add(new TableFieldSchema().setName("userId").setType("STRING"));
        fields.add(new TableFieldSchema().setName("peersReceivedPex").setType("INTEGER"));
        fields.add(new TableFieldSchema().setName("version").setType("INTEGER"));
        TableSchema schema = new TableSchema().setFields(fields);
        return schema;
    }


    public static void start(Pipeline pipeline) throws IOException {
        try {
            PCollection<TableRow> streamData = pipeline.apply(NAME + "_init", PubsubIO.readStrings()
                    .withTimestampAttribute("time")
                    .fromTopic(TOPIC))
                    .apply(PUBSUBNAME, ParDo.of(new JSONtoTableRow()))
                    .setIsBoundedInternal(PCollection.IsBounded.UNBOUNDED);
            // First write everything as we get it to BigQuery

            streamData.apply(NAME + "_BigQuery", BigQueryIO.writeTableRows()
                    .to(TABLESPEC)
                    .withSchema(getSchema())
                    .withCreateDisposition(BigQueryIO.Write.CreateDisposition.CREATE_IF_NEEDED)
                    .withWriteDisposition(BigQueryIO.Write.WriteDisposition.WRITE_APPEND));

            String rollupM = "1m";
            String rollupH = "1h";
            String rollupD = "1d";

            Duration lateness = Duration.standardMinutes(10);
            Duration durationHour = Duration.standardHours(WINDOW_SIZE);
            Duration durationDay = Duration.standardDays(WINDOW_SIZE);


            // minutes
            PCollection<Iterable<JsonDataStructure>> windowedM = streamData.apply(rollupM + "_CalculateConcurrentUsers", new CalculateConcurrent(rollupM));
            windowedM.apply(rollupM + "_map_concurrent", ParDo.of(new StringExporter()))
                    .apply(rollupM + "_concurrent_toPubSub", PubsubIO.writeStrings().to(TOPICPUBSUB));

            // branch of for hours
            PCollection<Iterable<JsonDataStructure>> windowedH = windowedM
                    .apply(rollupH + "_CalculateConcurrent", new AverageJsonDataStructures(durationHour, lateness, "concurrent", rollupH));

            // hours
            windowedH.apply(rollupH + "_map_concurrent", ParDo.of(new StringExporter()))
                    .apply(rollupH + "_concurrent_toPubSub", PubsubIO.writeStrings().to(TOPICPUBSUB));

            // branch of for days
            windowedH.apply(rollupD + "_CalculateConcurrent", new AverageJsonDataStructures(durationDay, lateness, "concurrent", rollupD))
                    .apply(rollupD + "_map_concurrent", ParDo.of(new StringExporter()))
                    .apply(rollupD + "_concurrent_toPubSub", PubsubIO.writeStrings().to(TOPICPUBSUB));


        } catch (Exception err) {
            LOG.trace(NAME + " Something wrong in the window", err);
        }

    }
}
