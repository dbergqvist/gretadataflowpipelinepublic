package com.greta.peer;

import com.greta.util.JsonDataStructure;
import org.apache.beam.sdk.transforms.DoFn;
import org.apache.beam.sdk.values.KV;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by bergqvist on 2017-08-29.
 */
public class ConcurrentDoFn extends DoFn<KV<String, Iterable<HashMap<String, String>>>, Iterable<JsonDataStructure>> {
    private static final Logger LOG = LoggerFactory.getLogger(ConcurrentDoFn.class);
    public String rollup;

    public ConcurrentDoFn(String rollup) {
        this.rollup = rollup;

    }

    @ProcessElement
    public void processElement(ProcessContext c) {
        HashMap<String, String> obj = c.element().getValue().iterator().next();
        String opentsdbKey = obj.get("accessToken") + ".user.concurrent.rollup";
        HashMap<String, String> tags = new HashMap<>();
        tags.put("geo", obj.get("geo"));
        tags.put("countryCode", obj.get("countryCode"));
        tags.put("continentCode", obj.get("continentCode"));
        tags.put("countryCode3", obj.get("countryCode3"));
        Float val;

        ArrayList<String> usersByLatLng = new ArrayList<>();
        for (HashMap<String, String> el : c.element().getValue()) {
            String userId = el.get("userId");
            if (!usersByLatLng.contains(userId)) {
                usersByLatLng.add(userId);
            }
        }

        val = (float) usersByLatLng.size();

        ArrayList<JsonDataStructure> res = new ArrayList<>();
        res.add(new JsonDataStructure(val, opentsdbKey, obj.get("time"), tags, rollup + ":SUM"));
        res.add(new JsonDataStructure(val, opentsdbKey, obj.get("time"), tags, rollup + ":COUNT"));


        if (rollup.equals("1m")) {
            res.add(new JsonDataStructure(val, obj.get("accessToken") + ".user.concurrent", obj.get("time"), tags, null));
        }
        LOG.info(res.toString());
        c.output(res);
    }
}

