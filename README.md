[Blog post](https://blog.greta.io/realtime-data-processing-using-google-cloud-and-opentsdb-c87a45ea3cc2) talking about the architecture and some comments about my involvement. 

**Dataflow branching**

[afterPeerPipeline.java](afterPeerPipeline.java)


    package com.greta;


    import com.google.api.services.bigquery.model.TableFieldSchema;
    import com.google.api.services.bigquery.model.TableRow;
    import com.google.api.services.bigquery.model.TableSchema;
    import com.greta.peer.ConcurrentDoFn;
    import com.greta.util.*;
    import org.apache.beam.sdk.Pipeline;
    import org.apache.beam.sdk.io.gcp.bigquery.BigQueryIO;
    import org.apache.beam.sdk.io.gcp.pubsub.PubsubIO;
    import org.apache.beam.sdk.transforms.GroupByKey;
    import org.apache.beam.sdk.transforms.PTransform;
    import org.apache.beam.sdk.transforms.ParDo;
    import org.apache.beam.sdk.transforms.windowing.SlidingWindows;
    import org.apache.beam.sdk.transforms.windowing.Window;
    import org.apache.beam.sdk.values.PCollection;
    import org.joda.time.Duration;
    import org.slf4j.Logger;
    import org.slf4j.LoggerFactory;

    import java.io.IOException;
    import java.util.ArrayList;
    import java.util.HashMap;
    import java.util.List;

    public class PeerPipeline {
        private static final Logger LOG = LoggerFactory.getLogger(PeerPipeline.class);
        private static final String NAME = "peer";
        private static final int WINDOW_SIZE = 1;

        private static final String BIGQUERYTABLE = NAME + "Day";
        private static final String TABLESPEC = Start.PROJECTID + ":" + Start.DATASET + "." + BIGQUERYTABLE;
        private static final String PUBSUBNAME = "Read_" + NAME;
        private static final String TOPIC = "projects/" + Start.PROJECTID + "/topics/" + NAME;
        private static final String TOPICPUBSUB = ProjectTopicName.of(PROJECT_ID, topicId);

        static class CalculateConcurrent extends PTransform<PCollection<TableRow>, PCollection<Iterable<JsonDataStructure>>> {
            private final String rollup;

            CalculateConcurrent(String rollup) {
                this.rollup = rollup;
            }

            @Override
            public PCollection<Iterable<JsonDataStructure>> expand(PCollection<TableRow> infos) {
                PCollection<TableRow> res = infos.apply(rollup + "CalculateConcurrentSlidingWindows",
                        Window.<TableRow>into(SlidingWindows.of(
                                Duration.standardMinutes(4))));
                return res.apply(rollup + "_byAcc_concurrent", ParDo.of(new MapByPeer()))
                        .apply(rollup + "_groupbykey_concurrent", GroupByKey.<String, HashMap<String, String>>create())
                        .apply(rollup + "_pardo_concurrent", ParDo.of(new ConcurrentDoFn(rollup)));

            }
        }

        private static TableSchema getSchema() {
            List<TableFieldSchema> fields = new ArrayList<>();
            fields.add(new TableFieldSchema().setName("avgRtt").setType("INTEGER"));
            fields.add(new TableFieldSchema().setName("avgBandwidthP2P").setType("INTEGER"));
            fields.add(new TableFieldSchema().setName("avgBandwidthServer").setType("INTEGER"));
            fields.add(new TableFieldSchema().setName("avgConnectionTime").setType("INTEGER"));
            fields.add(new TableFieldSchema().setName("openConnections").setType("INTEGER"));
            fields.add(new TableFieldSchema().setName("failedConnections").setType("INTEGER"));
            fields.add(new TableFieldSchema().setName("timeouts").setType("INTEGER"));
            fields.add(new TableFieldSchema().setName("trackCallTime").setType("INTEGER"));
            fields.add(new TableFieldSchema().setName("isp").setType("STRING"));
            fields.add(new TableFieldSchema().setName("lat").setType("INTEGER"));
            fields.add(new TableFieldSchema().setName("lng").setType("INTEGER"));
            fields.add(new TableFieldSchema().setName("accessToken").setType("STRING"));
            fields.add(new TableFieldSchema().setName("time").setType("TIMESTAMP"));
            fields.add(new TableFieldSchema().setName("userId").setType("STRING"));
            fields.add(new TableFieldSchema().setName("peersReceivedPex").setType("INTEGER"));
            fields.add(new TableFieldSchema().setName("version").setType("INTEGER"));
            TableSchema schema = new TableSchema().setFields(fields);
            return schema;
        }


        public static void start(Pipeline pipeline) throws IOException {
            try {
                PCollection<TableRow> streamData = pipeline.apply(NAME + "_init", PubsubIO.readStrings()
                        .withTimestampAttribute("time")
                        .fromTopic(TOPIC))
                        .apply(PUBSUBNAME, ParDo.of(new JSONtoTableRow()))
                        .setIsBoundedInternal(PCollection.IsBounded.UNBOUNDED);
                // First write everything as we get it to BigQuery

                streamData.apply(NAME + "_BigQuery", BigQueryIO.writeTableRows()
                        .to(TABLESPEC)
                        .withSchema(getSchema())
                        .withCreateDisposition(BigQueryIO.Write.CreateDisposition.CREATE_IF_NEEDED)
                        .withWriteDisposition(BigQueryIO.Write.WriteDisposition.WRITE_APPEND));

                String rollupM = "1m";
                String rollupH = "1h";
                String rollupD = "1d";

                Duration lateness = Duration.standardMinutes(10);
                Duration durationHour = Duration.standardHours(WINDOW_SIZE);
                Duration durationDay = Duration.standardDays(WINDOW_SIZE);


                // minutes
                PCollection<Iterable<JsonDataStructure>> windowedM = streamData.apply(rollupM + "_CalculateConcurrentUsers", new CalculateConcurrent(rollupM));
                windowedM.apply(rollupM + "_map_concurrent", ParDo.of(new StringExporter()))
                        .apply(rollupM + "_concurrent_toPubSub", PubsubIO.writeStrings().to(TOPICPUBSUB));

                // branch of for hours
                PCollection<Iterable<JsonDataStructure>> windowedH = windowedM
                        .apply(rollupH + "_CalculateConcurrent", new AverageJsonDataStructures(durationHour, lateness, "concurrent", rollupH));

                // hours
                windowedH.apply(rollupH + "_map_concurrent", ParDo.of(new StringExporter()))
                        .apply(rollupH + "_concurrent_toPubSub", PubsubIO.writeStrings().to(TOPICPUBSUB));

                // branch of for days
                windowedH.apply(rollupD + "_CalculateConcurrent", new AverageJsonDataStructures(durationDay, lateness, "concurrent", rollupD))
                        .apply(rollupD + "_map_concurrent", ParDo.of(new StringExporter()))
                        .apply(rollupD + "_concurrent_toPubSub", PubsubIO.writeStrings().to(TOPICPUBSUB));


            } catch (Exception err) {
                LOG.trace(NAME + " Something wrong in the window", err);
            }

        }
    }


[AverageJsonDataStructures.java](AverageJsonDataStructures.java)

    package com.greta.util;

    import org.apache.beam.sdk.transforms.GroupByKey;
    import org.apache.beam.sdk.transforms.PTransform;
    import org.apache.beam.sdk.transforms.ParDo;
    import org.apache.beam.sdk.transforms.windowing.AfterProcessingTime;
    import org.apache.beam.sdk.transforms.windowing.FixedWindows;
    import org.apache.beam.sdk.transforms.windowing.Window;
    import org.apache.beam.sdk.values.PCollection;
    import org.joda.time.Duration;

    /**
    * Created by bergqvist on 2017-10-11.
    */
    public class AverageJsonDataStructures extends PTransform<PCollection<Iterable<JsonDataStructure>>, PCollection<Iterable<JsonDataStructure>>> {

        private final Duration duration;
        private final Duration lateness;
        private final String name;
        private final String rollup;

        public AverageJsonDataStructures(Duration windowDuration, Duration lateness, String name, String rollup) {
            this.duration = windowDuration;
            this.lateness = lateness;
            this.name = name;
            this.rollup = rollup;
        }

        @Override
        public PCollection<Iterable<JsonDataStructure>> expand(PCollection<Iterable<JsonDataStructure>> iterables) {
            return iterables.apply(rollup + "_window_" + name, Window.<Iterable<JsonDataStructure>>into(FixedWindows.of(duration))
                    .triggering(AfterProcessingTime.pastFirstElementInPane()
                            .plusDelayOf(duration))
                    .discardingFiredPanes()
                    .withAllowedLateness(lateness))
                    .apply(rollup + "_map_" + name, ParDo.of(new MapByRollup()))
                    .apply(rollup + "group" + name, GroupByKey.<String, JsonDataStructure>create())
                    .apply(rollup + "_avg_" + name, ParDo.of(new AvgByKey()));
        }
    }


[AvgByKey.java](AvgByKey.java)

    package com.greta.util;

    import org.apache.beam.sdk.transforms.DoFn;
    import org.apache.beam.sdk.values.KV;
    import org.slf4j.Logger;
    import org.slf4j.LoggerFactory;

    import java.util.Arrays;

    /**
    * Created by bergqvist on 2017-10-11.
    */
    public class AvgByKey extends DoFn<KV<String, Iterable<JsonDataStructure>>, Iterable<JsonDataStructure>> {
        private static final Logger LOG = LoggerFactory.getLogger(SumByKey.class);

        @ProcessElement
        public void processElement(ProcessContext c) {
            Iterable<JsonDataStructure> values = c.element().getValue();
            // iterate all the jsonobj, average
            JsonDataStructure first = values.iterator().next();

            if (!first.rollupSpec.isEmpty() && !first.rollupSpec.equals("null")) {
                String rollup = first.rollupSpec.replace("1h", "1d");
                rollup = rollup.replace("1m", "1h");
                JsonDataStructure res = new JsonDataStructure(0f, first.openTSDBKey, first.time, first.tags, rollup);

                Integer count = 0;
                for (JsonDataStructure obj : values) {
                    res.value = res.value + obj.value;
                    count = count + 1;
                }

                res.value = res.value / count;
                LOG.info("avgbykey, " + first.openTSDBKey + ", rollup from " + first.rollupSpec + " -> " + rollup);
                c.output(Arrays.asList(res));
            }
        }
    }


[beforePeerPipeline.java](beforePeerPipeline.java)

    package com.greta;


    import com.google.api.services.bigquery.model.TableFieldSchema;
    import com.google.api.services.bigquery.model.TableRow;
    import com.google.api.services.bigquery.model.TableSchema;
    import com.greta.util.*;
    import org.apache.beam.sdk.Pipeline;
    import org.apache.beam.sdk.io.gcp.bigquery.BigQueryIO;
    import org.apache.beam.sdk.io.gcp.pubsub.PubsubIO;
    import org.apache.beam.sdk.transforms.ParDo;
    import org.apache.beam.sdk.values.PCollection;
    import org.slf4j.Logger;
    import org.slf4j.LoggerFactory;

    import java.io.IOException;
    import java.util.ArrayList;
    import java.util.List;

    public class PeerPipeline {
        private static final Logger LOG = LoggerFactory.getLogger(PeerPipeline.class);
        private static final String NAME = "peer";

        private static final String BIGQUERYTABLE = NAME + "Day";
        private static final String TABLESPEC = Start.PROJECTID + ":" + Start.DATASET + "." + BIGQUERYTABLE;
        private static final String PUBSUBNAME = "Read_" + NAME;
        private static final String TOPIC = "projects/" + Start.PROJECTID + "/topics/" + NAME;

        private static TableSchema getSchema() {
            List<TableFieldSchema> fields = new ArrayList<>();
            fields.add(new TableFieldSchema().setName("avgRtt").setType("INTEGER"));
            fields.add(new TableFieldSchema().setName("avgBandwidthP2P").setType("INTEGER"));
            fields.add(new TableFieldSchema().setName("avgBandwidthServer").setType("INTEGER"));
            fields.add(new TableFieldSchema().setName("avgConnectionTime").setType("INTEGER"));
            fields.add(new TableFieldSchema().setName("openConnections").setType("INTEGER"));
            fields.add(new TableFieldSchema().setName("failedConnections").setType("INTEGER"));
            fields.add(new TableFieldSchema().setName("timeouts").setType("INTEGER"));
            fields.add(new TableFieldSchema().setName("trackCallTime").setType("INTEGER"));
            fields.add(new TableFieldSchema().setName("isp").setType("STRING"));
            fields.add(new TableFieldSchema().setName("lat").setType("INTEGER"));
            fields.add(new TableFieldSchema().setName("lng").setType("INTEGER"));
            fields.add(new TableFieldSchema().setName("accessToken").setType("STRING"));
            fields.add(new TableFieldSchema().setName("time").setType("TIMESTAMP"));
            fields.add(new TableFieldSchema().setName("userId").setType("STRING"));
            fields.add(new TableFieldSchema().setName("peersReceivedPex").setType("INTEGER"));
            fields.add(new TableFieldSchema().setName("version").setType("INTEGER"));
            TableSchema schema = new TableSchema().setFields(fields);
            return schema;
        }


        public static void start(Pipeline pipeline) throws IOException {
            try {
                PCollection<TableRow> streamData = pipeline.apply(NAME + "_init", PubsubIO.readStrings()
                        .withTimestampAttribute("time")
                        .fromTopic(TOPIC))
                        .apply(PUBSUBNAME, ParDo.of(new JSONtoTableRow()))
                        .setIsBoundedInternal(PCollection.IsBounded.UNBOUNDED);
                // First write everything as we get it to BigQuery

                streamData.apply(NAME + "_BigQuery", BigQueryIO.writeTableRows()
                        .to(TABLESPEC)
                        .withSchema(getSchema())
                        .withCreateDisposition(BigQueryIO.Write.CreateDisposition.CREATE_IF_NEEDED)
                        .withWriteDisposition(BigQueryIO.Write.WriteDisposition.WRITE_APPEND));

            } catch (Exception err) {
                LOG.trace(NAME + " Something wrong in the window", err);
            }

        }
    }


![Alt text](beforePipelineStructure.png)


[ConcurrentDoFn.java](ConcurrentDoFn.java)



    package com.greta.peer;

    import com.greta.util.JsonDataStructure;
    import org.apache.beam.sdk.transforms.DoFn;
    import org.apache.beam.sdk.values.KV;
    import org.slf4j.Logger;
    import org.slf4j.LoggerFactory;

    import java.util.ArrayList;
    import java.util.HashMap;

    /**
    * Created by bergqvist on 2017-08-29.
    */
    public class ConcurrentDoFn extends DoFn<KV<String, Iterable<HashMap<String, String>>>, Iterable<JsonDataStructure>> {
        private static final Logger LOG = LoggerFactory.getLogger(ConcurrentDoFn.class);
        public String rollup;

        public ConcurrentDoFn(String rollup) {
            this.rollup = rollup;

        }

        @ProcessElement
        public void processElement(ProcessContext c) {
            HashMap<String, String> obj = c.element().getValue().iterator().next();
            String opentsdbKey = obj.get("accessToken") + ".user.concurrent.rollup";
            HashMap<String, String> tags = new HashMap<>();
            tags.put("geo", obj.get("geo"));
            tags.put("countryCode", obj.get("countryCode"));
            tags.put("continentCode", obj.get("continentCode"));
            tags.put("countryCode3", obj.get("countryCode3"));
            Float val;

            ArrayList<String> usersByLatLng = new ArrayList<>();
            for (HashMap<String, String> el : c.element().getValue()) {
                String userId = el.get("userId");
                if (!usersByLatLng.contains(userId)) {
                    usersByLatLng.add(userId);
                }
            }

            val = (float) usersByLatLng.size();

            ArrayList<JsonDataStructure> res = new ArrayList<>();
            res.add(new JsonDataStructure(val, opentsdbKey, obj.get("time"), tags, rollup + ":SUM"));
            res.add(new JsonDataStructure(val, opentsdbKey, obj.get("time"), tags, rollup + ":COUNT"));


            if (rollup.equals("1m")) {
                res.add(new JsonDataStructure(val, obj.get("accessToken") + ".user.concurrent", obj.get("time"), tags, null));
            }
            LOG.info(res.toString());
            c.output(res);
        }
    }


![Alt text](desiredStructureToTheLeft.png)