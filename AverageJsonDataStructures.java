package com.greta.util;

import org.apache.beam.sdk.transforms.GroupByKey;
import org.apache.beam.sdk.transforms.PTransform;
import org.apache.beam.sdk.transforms.ParDo;
import org.apache.beam.sdk.transforms.windowing.AfterProcessingTime;
import org.apache.beam.sdk.transforms.windowing.FixedWindows;
import org.apache.beam.sdk.transforms.windowing.Window;
import org.apache.beam.sdk.values.PCollection;
import org.joda.time.Duration;

/**
 * Created by bergqvist on 2017-10-11.
 */
public class AverageJsonDataStructures extends PTransform<PCollection<Iterable<JsonDataStructure>>, PCollection<Iterable<JsonDataStructure>>> {

    private final Duration duration;
    private final Duration lateness;
    private final String name;
    private final String rollup;

    public AverageJsonDataStructures(Duration windowDuration, Duration lateness, String name, String rollup) {
        this.duration = windowDuration;
        this.lateness = lateness;
        this.name = name;
        this.rollup = rollup;
    }

    @Override
    public PCollection<Iterable<JsonDataStructure>> expand(PCollection<Iterable<JsonDataStructure>> iterables) {
        return iterables.apply(rollup + "_window_" + name, Window.<Iterable<JsonDataStructure>>into(FixedWindows.of(duration))
                .triggering(AfterProcessingTime.pastFirstElementInPane()
                        .plusDelayOf(duration))
                .discardingFiredPanes()
                .withAllowedLateness(lateness))
                .apply(rollup + "_map_" + name, ParDo.of(new MapByRollup()))
                .apply(rollup + "group" + name, GroupByKey.<String, JsonDataStructure>create())
                .apply(rollup + "_avg_" + name, ParDo.of(new AvgByKey()));
    }
}

